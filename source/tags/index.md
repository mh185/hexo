---
title: 标签
date: 2020-11-16 00:29:48
---

# 这是标签页

---

### 不知道写啥

{% note default %}
default 提示标签符
{% endnote %}

{% note success %}
success 提示标签符
{% endnote %}

{% note info %}
info 提示标签符
{% endnote %}

{% note warning %}
warning 提示标签符
{% endnote %}

{% note danger %}
danger 提示标签符
{% endnote %}

{% gallery %}
markdown 图片格式
{% endgallery %}

<div class="gallery-group-main">
{% galleryGroup name description link img-url %}
{% galleryGroup name description link img-url %}
{% galleryGroup name description link img-url %}
</div>
